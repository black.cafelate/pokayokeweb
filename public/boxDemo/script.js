document.addEventListener('DOMContentLoaded', function() {
    const speedRange = document.getElementById('speedRange');
    const speedDisplay = document.getElementById('speedDisplay');
    const boxes = document.querySelectorAll('.box');

    function updateSpeedDisplay(speedValue) {
        const distance = 1; // メートル
        const duration = 5 / speedValue; // スライダーの値に基づく持続時間（秒）
        const speedMetersPerSecond = distance / duration;
        const speedMetersPerMinute = speedMetersPerSecond * 60; // メートル/秒をメートル/分に変換
        speedDisplay.textContent = `Speed: ${speedMetersPerMinute.toFixed(2)} m/min`; // 分単位で表示
    }

    function applyAnimation(speed) {
        const speedValue = parseFloat(speed);
        boxes.forEach((box, index) => {
            let delay = index * 1000 / speedValue;
            box.style.animation = `moveBox ${5 / speedValue}s linear ${delay}ms infinite`;

            JsBarcode(box.querySelector('.barcode'), `Box${index+1}`, {
                format: "CODE128",
                lineColor: "#000",
                width: 2,
                height: 40,
                displayValue: true
            });
        });
        updateSpeedDisplay(speedValue);
    }

    applyAnimation(speedRange.value);

    speedRange.addEventListener('input', function() {
        applyAnimation(this.value);
    });
});
